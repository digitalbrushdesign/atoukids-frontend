$(".ak-filter-link a").click(function() {
    $(this).closest("section").toggleClass('ak-bg-advanced');
    if(window.matchMedia("(max-width: 480px)").matches){
        $(".nav-pills-dropdown li:first-child").addClass("active");
    }
});


$('.nav-pills-dropdown')
.on("click", "li:not('.active')", function(event) {
    $(this).closest('ul').removeClass("open");
    $(this).addClass("active");
    $(this).siblings().removeClass("active");
})
.on("click", "li:has('.active')", function(event) {
    $(this).closest('ul').addClass("open");
});


  $("input[type=checkbox]").on('change', function () {
    if ($(this).prop('checked')) {
        $(this).parent().toggleClass("selected");
    }else{
        $(this).parent().removeClass("selected");
    }
        
  });

  $(".collapse").on('show.bs.collapse', function(){
    $(this).parent().addClass("ak-onboard-flex");
  });

  $(".collapse").on('hidden.bs.collapse', function(){
    $(this).parent().removeClass("ak-onboard-flex");
  });

  var swiper = new Swiper('.ak-why-carousel', {
    freeMode: true,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    breakpoints: {
        480: {
            slidesPerView: 1,
            spaceBetween: 20,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 20,
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
        1140: {
            slidesPerView: 5,
            spaceBetween: 30,
        },
      }
  });

  var swiper = new Swiper('.ak-who-carousel', {
    spaceBetween: 30,
    centeredSlides: true,
    loop: true,
    paginationClickable: true,
    watchSlidesVisibility: true,
    effect: 'coverflow',
    coverflowEffect: {
        rotate: 0,
        stretch: 100,
        depth: 160,
        modifier: 2,
        slideShadows : false,
    },
    autoplay: {
        delay: 5000,
        disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    slidesPerView: 'auto'
  });

  var swiper = new Swiper('.ak-page-reviews', {
    centeredSlides: true,
    loop: true,
    paginationClickable: true,
    watchSlidesVisibility: true,
    effect: 'coverflow',
    coverflowEffect: {
        rotate: 0,
        stretch: 100,
        depth: 205,
        modifier: 3,
        slideShadows : false,
    },
    autoplay: {
        delay: 5000,
        disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    slidesPerView: 'auto'
  });

  var swiper = new Swiper('.ak-profile-reviews', {
    centeredSlides: true,
    loop: true,
    paginationClickable: true,
    watchSlidesVisibility: true,
    effect: 'coverflow',
    coverflowEffect: {
        rotate: 0,
        stretch: 100,
        depth: 205,
        modifier: 3,
        slideShadows : false,
    },
    autoplay: {
        delay: 5000,
        disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    slidesPerView: 'auto'
  });

  var swiper = new Swiper('.ak-profile-messages', {
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      480: {
          slidesPerView: 1,
          spaceBetween: 50,
      },
      640: {
        slidesPerView: 2,
        spaceBetween: 100,
      },
    },
  });

  var galleryThumbs = new Swiper('.ak-gallery-thumbs', {
    direction: 'vertical',
    spaceBetween: 10,
    slidesPerView: 4,
    breakpoints: {
      480: {
        direction: 'horizontal',
      },
      640: {
        direction: 'horizontal',
      },
      768: {
        direction: 'horizontal',
      },
      1024: {
        direction: 'vertical',
      },
    },
    loop: true,
    freeMode: true,
    loopedSlides: 5, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });

  var galleryTop = new Swiper('.ak-gallery-top', {
    spaceBetween: 10,
    loop: true,
    loopedSlides: 5, //looped slides should be the same
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    thumbs: {
      swiper: galleryThumbs,
    },
  });

  (function($) {
    $.fn.spinner = function() {
    this.each(function() {
    var el = $(this);
    
    // add elements
    el.wrap('<span class="spinner"></span>');     
    el.before('<span class="sub">-</span>');
    el.after('<span class="add">+</span>');
    
    // substract
    el.parent().on('click', '.sub', function () {
    if (el.val() > parseInt(el.attr('min')))
    el.val( function(i, oldval) { return --oldval; });
    });
    
    // increment
    el.parent().on('click', '.add', function () {
    if (el.val() < parseInt(el.attr('max')))
    el.val( function(i, oldval) { return ++oldval; });
    });
    });
    };
  })(jQuery);
    
    $('input[type=number]').spinner();

    // add row
    $("#addLanguageRow").click(function () {
        var html = '';
        html += '<div id="inputLanguageRow" class="dynamic-add-row">';
        html += '<label class="ak-dropdown">';
        html += '<select class="form-control" name="title[]" id="languageSelect">';
        html += '<option value="one">{option1}</option>';
        html += '<option value="one">{option1}</option>';
        html += '<option value="one">{option1}</option>';
        html += '</select>';
        html += '</label>';
        html += '<div class="input-group-append  ak-delete-button">';
        html += '<a id="removeLanguageRow" ><span class="btn btn-icon ak-delete-icon form-control"><i class="far fa-trash-alt"></i></span></a>';
        html += '</div>';
        html += '</div>';

        $('#newLanguageRow').append(html);
    });

    // remove row
    $(document).on('click', '#removeLanguageRow', function () {
        $(this).closest('#inputLanguageRow').remove();
    });



$(document).ready(function(){

  $('.year').datepicker({
        startView: "years",
        minViewMode: "years"
  });

  $('.day').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: "yyyy-mm-dd"
    });


  $('.month').datepicker({
        startView: "months",
        minViewMode: "months"
  }); 

            
 $('#addDiplomaRow').click(function(){

    var newel = $('#inputDiplomaRow:last').clone();             

  
    $(newel).insertAfter("#inputDiplomaRow:last");
 });

 $('#addDipFormationRow').click(function(){

    var newel = $('#inputDipFormationRow:last').clone();             

  
    $(newel).insertAfter("#inputDipFormationRow:last");
 });
   


}); 

var options = {
  title: 'Présentation de la confidentialité',
  message: "Nous utilisons des cookies pour améliorer votre expérience, à des fins d'analyse et pour vous montrer des offres adaptées à vos intérêts sur notre site et sur des sites tiers. Nous pouvons partager vos informations avec nos partenaires publicitaires et analytiques.",
  delay: 600,
  expires: 1,
  link: 'politique-de-confidentialite',
  onAccept: function(){
      var myPreferences = $.fn.ihavecookies.cookie();
      console.log("Vos préférences ont été enregistrées");
      console.log(myPreferences);
  },
  uncheckBoxes: true,
  acceptBtnLabel: "J'accept",
  advancedBtnLabel: 'Éditer',
  moreInfoLabel: 'Politique de confidentialité',
  cookieTypesTitle: "Sélectionnez les cookies que vous souhaitez accepter",
  fixedCookieTypeLabel: 'Nécessaire',
  fixedCookieTypeDesc: "Les cookies nécessaires sont absolument essentiels au bon fonctionnement du site Web."
}

$(document).ready(function() {
  $('body').ihavecookies(options);

  $('#ihavecookiesBtn').on('click', function(){
      $('body').ihavecookies(options, 'reinit');
  });
});

